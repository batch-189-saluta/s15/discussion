// For outputting data or text into the browser console
console.log('Hello World')

// [SYNTAX AND STATEMENTS]
// Syntax is the code that makes up as statement
// console
// log()
// Statements are made up of syntax that will be run by the browser
// console.log()


// [COMMENTS]
// this is a single-line comment for short description

/*
	this is a multi-line comment (ctrl + shift + /)
*/

// top to bottom ang basa ng browser


// [VARIABLES]
// let variables are variables that can be reassigned
let firstName = 'Earl';
console.log(firstName);

let	lastName = 'Musk';
console.log(lastName);

// re-assigning a value to a let variable shows no errors
firstName = 'Elon';
console.log(firstName);

// const variables are variables that CANNOT be re-assigned
const colorOfTheSun = 'yellow';
console.log('The color of the sun is ' + colorOfTheSun);

// error happens when you try to reassign value of a const variable
/*colorOfTheSun = 'red';
console.log(colorOfTheSun);*/

// when declaring a variable, use a keyword like 'let'
let variableName = 'value';

// when re-assigning a value to a variable, you just need the variable name
variableName = 'New Value';


/*data types
	strings - 'texts' / 'alphanumeric characters' (dapat may '')
	numbers - positive, negative, with decimals, etc
	boolean - true or false
	null - absence of value (minsan ginagamit pang place 
	holder kasi walang value)
	object - set of information 
		[] - array (purely values)
		{} - object (may value name. mas specific)

		string,number,boolean is PRIMITIVE 
		because it can only hold a single value

		object is COMPOSITE data because it can hold multiple data 
		(arrays)
		
		null and undefined are considered SPECIAL 
		due to what it represents

*/

// data types
// 1. string - denoted by single OR double quotation marks 
let personName = 'Earl Diaz';

// 2. Numbers - no quotation marks and numerical value
let personAge = 15;

// 3. boolean - only true or false
let hasGirlfriend = false;

// 4. array - denoted by brackets and can contain multiple values
let hobbies = ['cycling', 'reading', 'coding'];

// 5. Object - denoted by curly braces, contains value name/label for each value
let person = {
	personName: 'Earl Diaz',
	personAge: 15,
	hasGirlfriend: false,
	hobbies: ['cycling', 'reading', 'coding'],

}

// Null - a place holder for future variable re-assignment
let wallet = null

// console.log each of the variables
console.log(personName);
console.log(personAge);
console.log(hasGirlfriend);
console.log(hobbies);
console.log(person);
console.log(wallet);

// to display single value of an object
console.log(person.personAge)

// to display single value of an array (laging magstart ang bilang from zero)
console.log(hobbies[1]);